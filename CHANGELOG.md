# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.4.3](https://github.com/Ygones-linear/projects-form/compare/v0.4.2...v0.4.3) (2021-02-23)

### [0.4.2](https://github.com/Ygones-linear/projects-form/compare/v0.4.1...v0.4.2) (2021-02-12)


### Bug Fixes

* bad folder to deploy public website ([276f488](https://github.com/Ygones-linear/projects-form/commit/276f488ec66160540b3517cd725532dcd4c94a16))

### [0.4.1](https://github.com/Ygones-linear/projects-form/compare/v0.4.0...v0.4.1) (2021-02-12)


### Bug Fixes

* add package-lock to auto-deploy on release ([e91aae7](https://github.com/Ygones-linear/projects-form/commit/e91aae722871b4cb717ed2a765b3803a5dc02d93))

## [0.4.0](https://github.com/Ygones-linear/projects-form/compare/v0.3.1...v0.4.0) (2021-02-12)


### Bug Fixes

* config to deploy website ([22a70a8](https://github.com/Ygones-linear/projects-form/commit/22a70a8b1e342964df7d354e470b385c5d3db6e6))
* script to release ([2d05f72](https://github.com/Ygones-linear/projects-form/commit/2d05f729996cf3437b5052adf4f4d076368ceea6))

### [0.3.1](https://github.com/Ygones-linear/projects-form/compare/v0.3.0...v0.3.1) (2021-02-05)

## [0.3.0](https://github.com/Ygones-linear/projects-form/compare/v0.2.0...v0.3.0) (2021-02-04)

## 0.2.0 (2021-02-04)


### Features

* add bootstrap ([be9ace8](https://github.com/Ygones-linear/projects-form/commit/be9ace8581b8b357d0b85d31598c1082369931ee))
* add script to release ([aa23d0b](https://github.com/Ygones-linear/projects-form/commit/aa23d0b276658c2ea48a148d70afada8f9a7d652))


### Bug Fixes

* bootstrap ([5bcdcbf](https://github.com/Ygones-linear/projects-form/commit/5bcdcbfee7d94216d5978c515c8c7d1dc16df60b))
* remove *-lock.json of git ([6b85a63](https://github.com/Ygones-linear/projects-form/commit/6b85a6397995ddbd74c013fac277f0561a1881d9))
