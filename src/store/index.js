import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    questions: [],
    user: {
      loggedIn: false,
      data: null
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  },
  mutations: {
    SET_LOGGED_IN (state, value) {
      state.user.loggedIn = value
    },
    SET_USER (state, data) {
      state.user.data = data
    },
    PUSH_QUESTION: (state, question) => {
      state.questions.push(question)
    },
    REMOVE_QUESTION: (state, index) => {
      state.questions.splice(index, 1)
    }
  },
  actions: {
    addQuestion: function ({ commit }, question) {
      if (question.title && question.responses.length > 0) {
        commit('PUSH_QUESTION', question)
      }
    },
    removeQuestion: function ({ commit, state }, question) {
      const i = state.questions.indexOf(question)
      if (i !== -1) {
        commit('REMOVE_QUESTION', i)
      }
    },
    addResponse: function ({ commit }, question, response) {
    },
    fetchUser ({ commit }, user) {
      commit('SET_LOGGED_IN', user !== null)
      if (user) {
        commit('SET_USER', {
          displayName: user.displayName,
          email: user.email
        })
      } else {
        commit('SET_USER', null)
      }
    }
  },
  modules: {
  }
})
