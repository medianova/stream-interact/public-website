import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import styles from './assets/scss/styles.scss'
import firebase from 'firebase/app'
require('firebase/auth')
require('bootstrap')

Vue.config.productionTip = false

export const firebaseConfig = {
  apiKey: 'AIzaSyCdYPaJst5fUUEtoj1kROI3kxXaBzAPCGU',
  authDomain: 'twitch-interractive.firebaseapp.com',
  databaseURL: 'https://twitch-interractive.firebaseio.com',
  projectId: 'twitch-interractive',
  storageBucket: 'twitch-interractive.appspot.com',
  messagingSenderId: '938863285656',
  appId: '1:938863285656:web:76d62c7be69d6ccca407ed',
  measurementId: 'G-81NZXEZF45'
}

firebase.initializeApp(firebaseConfig)

firebase.auth().onAuthStateChanged(user => {
  store.dispatch('fetchUser', user)
})

export const db = firebase

new Vue({
  styles,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
