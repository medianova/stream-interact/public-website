# Script to install prod
install:
	yarn && yarn build

# from develop to master
release:
	yarn && yarn build
	./scripts/release.sh

release-minor:
	yarn && yarn build
	./scripts/release-minor.sh

release-major:
	yarn && yarn build
	./scripts/release-major.sh
